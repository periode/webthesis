#!/usr/bin/env bash

echo "// this code is auto generated"
echo ""

# write imports
for file in ./src/corpus/*; do
	fname=$(basename $file)
	echo "import "${fname/./_}" from \"../corpus/"$fname"?raw\""
done

# start switch statement
cat <<'END'

export const getRawSourceCode = (key: string): string => {
	switch (key) {
END

# write cases
for file in ./src/corpus/*; do
	fname=$(basename $file)
	echo "		case \""$fname"\":"
	echo "			return "${fname/./_}
done

# conclude
cat <<'END'
		default:
         		console.warn(`no source available for ${key}`)
			return `no source available for ${key}`
	}
}
END
