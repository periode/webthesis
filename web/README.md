# Webthesis - web

This is the web interface to render the parsed tree of a series of `.tex` files, exported by the [parser](../parser/README.md).

Each `.tex` file is exported as a `.json` file in the [data folder](./src/data/). Each file is essentially an array of nodes:

```js
let node = {
  value: string,
  tag: string,
  index: number,
  children: node[]
}
```

based on the `tag`, Sveltekit renders the node via a specific component (see [components](./src/components/)).
