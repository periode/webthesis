import { writable } from 'svelte/store'
import { browser } from '$app/environment';

//-- current_heading is used in the intersection observation
export const current_heading = writable('')

const theme = browser ? window.localStorage.theme as string : '';
export const dark_mode = writable(theme)

const font = browser ? window.localStorage.font ? window.localStorage.font as string : 'font-serif' : 'font-serif';
export const font_family = writable(font)

const length = browser ? window.localStorage['line-length'] ? window.localStorage['line-length'] as string : 'px-0' : 'px-0';
export const line_length = writable(length)

const height = browser ? window.localStorage['line-height'] ? window.localStorage['line-height'] as string : 'leading-normal' : 'leading-normal';
export const line_height = writable(height)

const size = browser ? window.localStorage['font-size'] ? window.localStorage['font-size'] as string : 'text-body' : 'text-body';
export const font_size = writable(size)

export const default_palette = {
    light: {
        text: "text-zinc-800",
        bg: "bg-zinc-50",
        border: "border-zinc-800"
    },
    dark: {
        text: "dark:text-zinc-100",
        bg: "dark:bg-zinc-800",
        border: "dark:border-zinc-100"
    }
}

function createPalette() {
    const p = browser ? window.localStorage['palette'] ? JSON.parse(window.localStorage['palette']) : default_palette : default_palette;
    const { subscribe, set } = writable(p);

    return {
        subscribe,
        update: (_pal: string) => {            
            let p  = {
                light: {
                    text: `text-${_pal}-800`,
                    bg: `bg-${_pal}-50`,
                    border: `border-${_pal}-800`
                },
                dark: {
                    text: `dark:text-${_pal}-100`,
                    bg: `dark:bg-${_pal}-800`,
                    border: `dark:border-${_pal}-100`
                }
            }
            set(p)
        },
        reset: () => set(default_palette)
    };
}

export const palette = createPalette();