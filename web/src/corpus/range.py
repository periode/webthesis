# idiomatic
for i in range(5):
  print(i)


# generic 
for i in [0, 1, 2, 3, 4, 5]:
    print(i)
