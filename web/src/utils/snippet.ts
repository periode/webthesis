// this code is auto generated

import alive_rb from "../corpus/alive.rb?raw"
import all_the_names_of_god_pl from "../corpus/all_the_names_of_god.pl?raw"
import all_the_names_of_god_txt from "../corpus/all_the_names_of_god.txt?raw"
import a_mind_is_born_hex from "../corpus/a_mind_is_born.hex?raw"
import a_out from "../corpus/a.out?raw"
import binary_java from "../corpus/binary.java?raw"
import black_perl_pl from "../corpus/black_perl.pl?raw"
import bubblesort_jl from "../corpus/bubblesort.jl?raw"
import buffer_c from "../corpus/buffer.c?raw"
import circle_c from "../corpus/circle.c?raw"
import clearer_method_c from "../corpus/clearer_method.c?raw"
import clipboard_js from "../corpus/clipboard.js?raw"
import compressed_c from "../corpus/compressed.c?raw"
import cynical_american_preamble_py from "../corpus/cynical_american_preamble.py?raw"
import enum_c from "../corpus/enum.c?raw"
import error_handling_go from "../corpus/error_handling.go?raw"
import factorial_c from "../corpus/factorial.c?raw"
import fast_inverse_sqrt_c from "../corpus/fast_inverse_sqrt.c?raw"
import fibonacci_py from "../corpus/fibonacci.py?raw"
import floyd_warshall_cpp from "../corpus/floyd_warshall.cpp?raw"
import forkbomb_pl from "../corpus/forkbomb.pl?raw"
import formatted_cpp from "../corpus/formatted.cpp?raw"
import game_of_life_apl from "../corpus/game_of_life.apl?raw"
import genalloc_c from "../corpus/genalloc.c?raw"
import hardware_separation_h from "../corpus/hardware_separation.h?raw"
import hello_go from "../corpus/hello.go?raw"
import hello_java from "../corpus/hello.java?raw"
import hello_rb from "../corpus/hello.rb?raw"
import home_js from "../corpus/home.js?raw"
import home_minified_js from "../corpus/home_minified.js?raw"
import illegal_return_c from "../corpus/illegal_return.c?raw"
import inductive_pl from "../corpus/inductive.pl?raw"
import interpreter_scheme from "../corpus/interpreter.scheme?raw"
import iterating_c from "../corpus/iterating.c?raw"
import iterating_py from "../corpus/iterating.py?raw"
import japh_pl from "../corpus/japh.pl?raw"
import level_asm from "../corpus/level.asm?raw"
import level_byte from "../corpus/level.byte?raw"
import level_c from "../corpus/level.c?raw"
import level_txt from "../corpus/level.txt?raw"
import linked_list_c from "../corpus/linked_list.c?raw"
import linked_list_h from "../corpus/linked_list.h?raw"
import mac_sched_c from "../corpus/mac_sched.c?raw"
import mesh_matlab from "../corpus/mesh.matlab?raw"
import ms2000_abridged_c from "../corpus/ms2000_abridged.c?raw"
import ms2000_c from "../corpus/ms2000.c?raw"
import multiple_references_c from "../corpus/multiple_references.c?raw"
import multiple_references_rs from "../corpus/multiple_references.rs?raw"
import multiple_returns_go from "../corpus/multiple_returns.go?raw"
import multiple_returns_js from "../corpus/multiple_returns.js?raw"
import nearest_neighbor_jl from "../corpus/nearest_neighbor.jl?raw"
import nested_html from "../corpus/nested.html?raw"
import non_thread_go from "../corpus/non_thread.go?raw"
import nsCSSRenderingBorders_cpp from "../corpus/nsCSSRenderingBorders.cpp?raw"
import numero_mysterioso_asm from "../corpus/numero_mysterioso.asm?raw"
import p5_js from "../corpus/p5.js?raw"
import pi_video_looper_py from "../corpus/pi_video_looper.py?raw"
import prince_java from "../corpus/prince.java?raw"
import pseudocode_txt from "../corpus/pseudocode.txt?raw"
import query_php from "../corpus/query.php?raw"
import range_py from "../corpus/range.py?raw"
import recursive_iteration_cs from "../corpus/recursive_iteration.cs?raw"
import references_c from "../corpus/references.c?raw"
import references_rb from "../corpus/references.rb?raw"
import regex_c from "../corpus/regex.c?raw"
import representation_java from "../corpus/representation.java?raw"
import representation_rs from "../corpus/representation.rs?raw"
import riddle_0x31_asm from "../corpus/riddle_0x31.asm?raw"
import route_php from "../corpus/route.php?raw"
import select_lines_c from "../corpus/select_lines.c?raw"
import select_lines_sh from "../corpus/select_lines.sh?raw"
import self_inspect_rb from "../corpus/self_inspect.rb?raw"
import self_inspect_txt from "../corpus/self_inspect.txt?raw"
import semaphore_cpp from "../corpus/semaphore.cpp?raw"
import shutdown_go from "../corpus/shutdown.go?raw"
import simple_py from "../corpus/simple.py?raw"
import smr_c from "../corpus/smr.c?raw"
import spatial_extension_py from "../corpus/spatial_extension.py?raw"
import test_py from "../corpus/test.py?raw"
import thread_c from "../corpus/thread.c?raw"
import thread_go from "../corpus/thread.go?raw"
import uncompressed_c from "../corpus/uncompressed.c?raw"
import unformatted_cpp from "../corpus/unformatted.cpp?raw"
import unhandled_love_java from "../corpus/unhandled_love.java?raw"
import unique_py from "../corpus/unique.py?raw"
import unmaintainable_2_c from "../corpus/unmaintainable_2.c?raw"
import unmaintainable_py from "../corpus/unmaintainable.py?raw"
import verbose_c from "../corpus/verbose.c?raw"
import verbose_refactored_c from "../corpus/verbose_refactored.c?raw"
import water_c from "../corpus/water.c?raw"
import water_out from "../corpus/water.out?raw"

export const getRawSourceCode = (key: string): string => {
	switch (key) {
		case "alive.rb":
			return alive_rb
		case "all_the_names_of_god.pl":
			return all_the_names_of_god_pl
		case "all_the_names_of_god.txt":
			return all_the_names_of_god_txt
		case "a_mind_is_born.hex":
			return a_mind_is_born_hex
		case "a.out":
			return a_out
		case "binary.java":
			return binary_java
		case "black_perl.pl":
			return black_perl_pl
		case "bubblesort.jl":
			return bubblesort_jl
		case "buffer.c":
			return buffer_c
		case "circle.c":
			return circle_c
		case "clearer_method.c":
			return clearer_method_c
		case "clipboard.js":
			return clipboard_js
		case "compressed.c":
			return compressed_c
		case "cynical_american_preamble.py":
			return cynical_american_preamble_py
		case "enum.c":
			return enum_c
		case "error_handling.go":
			return error_handling_go
		case "factorial.c":
			return factorial_c
		case "fast_inverse_sqrt.c":
			return fast_inverse_sqrt_c
		case "fibonacci.py":
			return fibonacci_py
		case "floyd_warshall.cpp":
			return floyd_warshall_cpp
		case "forkbomb.pl":
			return forkbomb_pl
		case "formatted.cpp":
			return formatted_cpp
		case "game_of_life.apl":
			return game_of_life_apl
		case "genalloc.c":
			return genalloc_c
		case "hardware_separation.h":
			return hardware_separation_h
		case "hello.go":
			return hello_go
		case "hello.java":
			return hello_java
		case "hello.rb":
			return hello_rb
		case "home.js":
			return home_js
		case "home_minified.js":
			return home_minified_js
		case "illegal_return.c":
			return illegal_return_c
		case "inductive.pl":
			return inductive_pl
		case "interpreter.scheme":
			return interpreter_scheme
		case "iterating.c":
			return iterating_c
		case "iterating.py":
			return iterating_py
		case "japh.pl":
			return japh_pl
		case "level.asm":
			return level_asm
		case "level.byte":
			return level_byte
		case "level.c":
			return level_c
		case "level.txt":
			return level_txt
		case "linked_list.c":
			return linked_list_c
		case "linked_list.h":
			return linked_list_h
		case "mac_sched.c":
			return mac_sched_c
		case "mesh.matlab":
			return mesh_matlab
		case "ms2000_abridged.c":
			return ms2000_abridged_c
		case "ms2000.c":
			return ms2000_c
		case "multiple_references.c":
			return multiple_references_c
		case "multiple_references.rs":
			return multiple_references_rs
		case "multiple_returns.go":
			return multiple_returns_go
		case "multiple_returns.js":
			return multiple_returns_js
		case "nearest_neighbor.jl":
			return nearest_neighbor_jl
		case "nested.html":
			return nested_html
		case "non_thread.go":
			return non_thread_go
		case "nsCSSRenderingBorders.cpp":
			return nsCSSRenderingBorders_cpp
		case "numero_mysterioso.asm":
			return numero_mysterioso_asm
		case "p5.js":
			return p5_js
		case "pi_video_looper.py":
			return pi_video_looper_py
		case "prince.java":
			return prince_java
		case "pseudocode.txt":
			return pseudocode_txt
		case "query.php":
			return query_php
		case "range.py":
			return range_py
		case "recursive_iteration.cs":
			return recursive_iteration_cs
		case "references.c":
			return references_c
		case "references.rb":
			return references_rb
		case "regex.c":
			return regex_c
		case "representation.java":
			return representation_java
		case "representation.rs":
			return representation_rs
		case "riddle_0x31.asm":
			return riddle_0x31_asm
		case "route.php":
			return route_php
		case "select_lines.c":
			return select_lines_c
		case "select_lines.sh":
			return select_lines_sh
		case "self_inspect.rb":
			return self_inspect_rb
		case "self_inspect.txt":
			return self_inspect_txt
		case "semaphore.cpp":
			return semaphore_cpp
		case "shutdown.go":
			return shutdown_go
		case "simple.py":
			return simple_py
		case "smr.c":
			return smr_c
		case "spatial_extension.py":
			return spatial_extension_py
		case "test.py":
			return test_py
		case "thread.c":
			return thread_c
		case "thread.go":
			return thread_go
		case "uncompressed.c":
			return uncompressed_c
		case "unformatted.cpp":
			return unformatted_cpp
		case "unhandled_love.java":
			return unhandled_love_java
		case "unique.py":
			return unique_py
		case "unmaintainable_2.c":
			return unmaintainable_2_c
		case "unmaintainable.py":
			return unmaintainable_py
		case "verbose.c":
			return verbose_c
		case "verbose_refactored.c":
			return verbose_refactored_c
		case "water.c":
			return water_c
		case "water.out":
			return water_out
		default:
         		console.warn(`no source available for ${key}`)
			return `no source available for ${key}`
	}
}
