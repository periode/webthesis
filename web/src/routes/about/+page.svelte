<script lang="ts">
  import {
    font_family,
    font_size,
    line_height,
    line_length,
  } from "../../stores";
</script>

<div
  class={`p-4 md:p-2 lg:p-8 ${$font_family} ${$line_height} ${$line_length} md:text-lg`}
>
  <div class="w-11/12 lg:w-5/12 md:w-6/12 m-auto indent-6">
    <h1 class="text-4xl m-auto mt-24 mb-16">About</h1>

    <h2 class="text-xl my-8 font-semibold">Overview</h2>
    <section class={`flex flex-col gap-2 ${$font_size} hyphens`}>
      <p>
        This is the web version of the PhD thesis of <a
          class="underline decoration-black/25 dark:decoration-white/50"
          href="https://pierredepaz.net"
          target="_blank">Pierre Depaz</a
        >, intending to facilitate access and readability of
        long-form written works beyond PDFs.
      </p>
      <p>
        As such, it is also an inquiry into publishing research work online. It
        integrates modular systems of epistemic software (<a
          class="underline decoration-black/25 dark:decoration-white/50"
          href="http://zotero.org"
          target="_blank"
          rel="noopener noreferrer">Zotero</a
        >,
        <a
          class="underline decoration-black/25 dark:decoration-white/50"
          href="http://hypothes.is"
          target="_blank"
          rel="noopener noreferrer">Hypothes.is</a
        >), hyperlinking of labelled components (headings, figures, listings,
        citations), document layout (user-accessible line height, line length,
        font family, font size, as well as responsive design and dark mode),
        asset delivery (optimizing requests for size and speed, manifesting as a
        <a
          class="underline decoration-black/25 dark:decoration-white/50"
          href="https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps"
          target="_blank"
          rel="noopener noreferrer">progressive web app</a
        >) and publishing pipeline (parsing, processing and templating).
      </p>
      <p>
        It is also a practical attempt at translating the ideas of this thesis
        in a different computational medium. Some aesthetic properties can
        support a positive cognitive engagement. As a different material,
        emphasis is being put on the referencing graph of different components
        (attempting to build up a habitable semantic space by connecting things
        meaningfully), on inteface design (enabling epistemic and navigational
        actions, such as highlighting and toggling content visibility), but also
        on more traditional document layout principles (typography, spacing).
      </p>
      <p>
        Others have also thought about and done things along the lines of <a
          class="underline decoration-black/25 dark:decoration-white/50"
          href="https://doi.org/10.1093/comjnl/27.2.97"
          target="_blank"
          rel="noopener noreferrer">literate programming</a
        >,
        <a
          class="underline decoration-black/25 dark:decoration-white/50"
          href="https://www.quaternum.net/"
          target="_blank"
          rel="noopener noreferrer">publication numérique</a
        >,
        <a
          class="underline decoration-black/25 dark:decoration-white/50"
          href="https://copim.pubpub.org/pub/computational-publishing/release/1?readingCollection=c2b231d4"
          target="_blank"
          rel="noopener noreferrer">computational publishing</a
        >,
        <a
          class="underline decoration-black/25 dark:decoration-white/50"
          href="https://xpub.nl/"
          target="_blank"
          rel="noopener noreferrer">experimental publishing</a
        >
        or even
        <a
          class="underline decoration-black/25 dark:decoration-white/50"
          href="https://journal.stuffwithstuff.com/2020/04/05/crafting-crafting-interpreters/"
          target="_blank"
          rel="noopener noreferrer">book crafting</a
        >. A lot of good ideas in a lot of different directions, getting deep
        into the malleability of digital media.
      </p>
      <p>
        If we were to take the conclusions of this PhD as a starting point, it
        turns out that some findings apply more, and some less, when translated
        to another environment. Some aesthetic properties of source code, such
        as metaphors and idiomaticity, apply more due to their evocative nature,
        and the high error-redundancy of the document. Metaphors happen mostly
        through the use of icons in order communicate the hybrid status of the
        document (partly text, partly software). Idiomaticity is related to the
        best practices of digital document design—in a way, speaking the
        language of the born-digital (still somewhat in becoming).
      </p>
      <p>
        On the other side, precise token naming and structuring of document are
        less fertile grounds for stylistic innovations. These differences are, I
        think, also due to a more evasive assessment of the executed text and
        its function. Because the text is also mostly traditionally read-only at
        the moment of encounter syntactic highlighting doesn't make as much
        sense in natural languages than in programming languages; it might even
        detract from the linear flow of natural language reading. In terms of
        linguistic register and prose style, it might not be the most
        accessible, due to the context in which it has been written, and a need
        to adapt to such context through idiomatic terminology; but it would be
        fun to re-write in more vernacular terms! Ultimately, though, this
        system tries do the most with the least—that is, it tries to be
        elegant—and tries to conciliate optimization for a specific instance and
        accomodation with novel data structures.
      </p>
    </section>

    <h2 class="text-xl my-8 font-semibold">Rationale</h2>
    <section class={`flex flex-col gap-2 ${$font_size} hyphens`}>
      <p>
        One of the requirement to fulfill this degree is to hand in a
        specifically formatted PDF document, a format which, I assumed, people
        tend not to read (or not to have access to, for paywall reasons). I also
        assumed that, as information keeps growing in importance in our
        societies, the quality of the information delivered becomes more clearly
        related to the means of diffusion. Media saturation could also imply
        media differentiation, even at the slightest level. Such quality is, in
        its turns, relatively more dependent on its formal presentation.
      </p>
      <p>
        What would be an appropriate medium for the presentation of this
        research? What does it look like to make a document more accessible as
        the result of media-translation? How much does the networked computer
        depart from print? And how can a static document be turned into a
        dynamic document? What are all implications of such a media translation?
      </p>
      <p>
        Here, I take a rather practical and processual approach to those
        questions, which means that there isn't much documentation and that it's
        an on-going process with no rigid sequence. On the other side, there's
        something to look at, comment on and play with.
      </p>
      <p>
        One of the conclusions of <a
          class="underline decoration-black/25 dark:decoration-white/50"
          href="/introduction">this thesis</a
        > is that a form of beauty of source code is the extent to which it is shaped
        adequately to its material. This raises the question of media-specificity.
        In this case, it is the relation between the content of the document, its
        medium of representation, and its representation through this medium. What
        are best (non-invasive, fast, connected) ways in which a digital page is
        different than a printed page?
      </p>
    </section>

    <h2 class="text-xl my-8 font-semibold">Details</h2>
    <section class={`flex flex-col gap-2 ${$font_size} hyphens`}>
      <p>
        The original document is written in <a
          class="underline decoration-black/25 dark:decoration-white/50"
          href="https://www.latex-project.org/"
          target="_blank"
          rel="noopener noreferrer">LaTeX</a
        >. The parsing is done in
        <a
          class="underline decoration-black/25 dark:decoration-white/50"
          href="https://doc.rust-lang.org/book/"
          target="_blank"
          rel="noopener noreferrer">Rust</a
        >
        with the
        <a
          class="underline decoration-black/25 dark:decoration-white/50"
          href="https://pest.rs/"
          target="_blank"
          rel="noopener noreferrer">Pest</a
        >
        parser, then a series of bash commands in a
        <a
          class="underline decoration-black/25 dark:decoration-white/50"
          href="https://www.gnu.org/software/make/"
          target="_blank"
          rel="noopener noreferrer">Makefile</a
        >
        copy the
        <a
          class="underline decoration-black/25 dark:decoration-white/50"
          href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON"
          target="_blank"
          rel="noopener noreferrer">JSON</a
        >
        output and required assets to a
        <a
          class="underline decoration-black/25 dark:decoration-white/50"
          href="https://svelte.dev/"
          target="_blank"
          rel="noopener noreferrer">Svelte</a
        >
        application; the app is then built by
        <a
          class="underline decoration-black/25 dark:decoration-white/50"
          href="https://kit.svelte.dev/"
          target="_blank"
          rel="noopener noreferrer">SvelteKit</a
        > and deployed to a virtual machine rented from Digital Ocean.
      </p>
      <p>
        The serif font is <a
          class="underline decoration-black/25 dark:decoration-white/50"
          href="https://www.fontshare.com/fonts/bespoke-serif"
          target="_blank"
          rel="noopener noreferrer">Bespoke</a
        >, the sans serif font is
        <a
          class="font-sans underline decoration-black/25 dark:decoration-white/50"
          href="https://rsms.me/inter/"
          target="_blank"
          rel="noopener noreferrer">Inter</a
        >
        the mono font is
        <a
          class="font-mono underline decoration-black/25 dark:decoration-white/50"
          href="https://docs.xz.style/fonts/ibm-plex/ibm-plex-mono"
          target="_blank"
          rel="noopener noreferrer">IBM Plex</a
        >
        and the icons are from
        <a
          class="underline decoration-black/25 dark:decoration-white/50"
          href="https://remixicon.com/"
          target="_blank"
          rel="noopener noreferrer">RemixIcon</a
        >.
      </p>
      <p>
        You can find the source code for this website (both LaTeX parser and HTML renderer) on <a
          class="underline decoration-black/25 dark:decoration-white/50"
          href="https://gitlab.com/periode/webthesis"
          target="_blank"
          rel="noopener noreferrer">this repository</a
        > and you can find the source files of the thesis itself on <a
        class="underline decoration-black/25 dark:decoration-white/50"
        href="https://gitlab.com/periode/thesis"
        target="_blank"
        rel="noopener noreferrer">this repository</a>.
      </p>
    </section>
    <div class="h-16" />
  </div>
</div>
