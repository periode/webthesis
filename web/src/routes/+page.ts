import type { PageLoad } from './$types';
import { findNodeByTag, findNodesByTag, getBundle, getToC } from "../utils/find";

export const load = (() => {
  const fp = getBundle("front");

  const t = findNodeByTag("title", fp)
  const title = t ? t.children ? t.children[0].value : "MISSING TITLE" : "MISSING TITLE"

  const au = findNodeByTag("author", fp)
  const author = au ? au.children ? au.children[0].value : "MISSING AUTHOR" : "MISSING AUTHOR"

  const af = findNodesByTag("affiliation", fp)
  const university = af && af.length > 1 ? af[0].children ? af[0].children[0].value : "MISSING AFFILIATION" : "MISSING AFFILIATION"
  const affiliation = af && af.length > 1 ? af[1].children ? af[1].children[0].value : "MISSING AFFILIATION" : "MISSING AFFILIATION"

  const d = findNodeByTag("date", fp)
  const date = d ? d.value : "MISSING DATE"

  const ab = findNodeByTag("abstract", fp)
  const abstract = ab ? ab.children ? ab.children[0].children ? ab.children[0].children[0].value : "MISSING ABSTRACT" : "MISSING ABSTRACT" : "MISSING ABSTRACT"

  const toc = getToC()

  return {
    title: title,
    author: author,
    affiliation: affiliation,
    university: university,
    date: date,
    abstract: abstract,
    toc: toc,
  };
}) satisfies PageLoad;
