import type { PageLoad } from './$types';
import { findHeadingValue, getSectionText, getBundle } from "../../../utils/find";

export const load = (({params}) => {    
    const root = getBundle(params.chap);
    const nodes = getSectionText(params.chap, params.sec, root);

    const chap_literal = findHeadingValue(`chap:${params.chap}`);
    
  return {
    nodes: nodes,
    chapter: chap_literal,
  };
}) satisfies PageLoad;