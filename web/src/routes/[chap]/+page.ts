import type { PageLoad } from './$types';
import { getChapterIncipit, getBundle, getToC } from "../../utils/find";

export const load = (({ params }) => {
  const chap = params.chap;
  const root = getBundle(chap);
  const nodes = getChapterIncipit(root)
  const toc = getToC(chap)

  return {
    nodes: nodes,
    toc: toc
  };
}) satisfies PageLoad;