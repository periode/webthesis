.DEFAULT_GOAL:=help
SHELL:=/usr/bin/bash

.PHONY: dev build upload all

thesis: ## parse the thesis source .tex file
	cd parser && cargo run -- -i /home/pierre/forschung/00_phd/redaction/thesis.tex -o output/thesis

data: thesis ## copy over all the source files, convert the bibliography and generate the snippets code
	cp ./parser/output/thesis/*.json ./web/src/data
	cp /home/pierre/forschung/00_phd/redaction/images/* ./web/static/images/figures
	cp /home/pierre/forschung/00_phd/redaction/corpus/* ./web/src/corpus
	cp /home/pierre/forschung/00_phd/redaction/thesis.pdf ./web/static/Depaz_AestheticsUnderstandingSourceCode.pdf
	cp /home/pierre/forschung/00_phd/redaction/thesis.bib ./web/static/Depaz_AestheticsUnderstandingSourceCode.bib
	pandoc ./web/static/Depaz_AestheticsUnderstandingSourceCode.bib -t csljson -o ./web/src/data/bib.json
	cd web/ && bash generator.sh > src/utils/snippet.ts

bib: ## copy the bibtex file and parse it to json
	cp /home/pierre/forschung/00_phd/redaction/thesis.bib ./web/static/Depaz_AestheticsUnderstandingSourceCode.bib
	pandoc ./web/static/Depaz_AestheticsUnderstandingSourceCode.bib -t csljson -o ./web/src/data/bib.json

dev: ## start the development server and open it in the browser
	cd web && npm run dev -- --open

build: ## compile the web application
	cd web && npm run build

upload: ## rsync the app to the remote host
	rsync -zur web/build/ enframed:/mnt/volume_nyc3_01/www/public/thesis

all: data build upload ## parse the source document, compile the app and upload it

help:  ## display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-10s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)
