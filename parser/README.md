# Webthesis - parser

This project takes a specific LaTeX file and returns an AST (abstract syntax tree), following a [grammar](./src/latex-grammar.pest) written in [Pest](https://pest.rs), which is then written as a series of JSON files. The output always includes the LaTeX entrypoint and any `\{include}` files.

## Notes

`Environment::Figure` refers to images, while `Environment::Listing` refers to code snippets (`Environment::Equation` is subsumed into `Environment::Listing`).

the parsing of `\item` is a bit tricky (it does not require `{}`, but takes the rest of the line as its body). the workaround is to adapt the writing of the `.tex` input to only use `\item{}`.

## Known issues

- [ ] Invisible LF characters break the parsing logic
- [ ] The `\href{}` element (url + display text) is not handled
- [ ] There should always be a new line at the end of the document, otherwise `\end{document}` does not get picked up
