# Webthesis

This project is a pipeline to render a collection of LaTeX files as a static website, taking advantage to modern web technologies in order to make research more accessible. It was originally written as a proof of concept for my PhD thesis, [the role of aesthetics in understanding source code](https://source.enframed.net).

The [parser](./parser/README.md) is written in Rust with a grammar written with Pest.

The [rendering](./web/README.md) is done in the Svelte flavor of HTML/CSS/JS.

You can read more about the thought process [here](NOTES.md).

Even though it was originally written in a somewhat bespoke way, merge requests are welcome to adapt it to other source files (since only subsets of LaTeX can e parsed).

For any questions, you can reach out to [pierre at enframed dot net](mailto:pierre@enframed.net)
